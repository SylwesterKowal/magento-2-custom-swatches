<?php
namespace M2it\CustomSwatches\Block\Adminhtml\Swatches\Edit;



class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

   
    protected $_systemStore;
    protected $_modelSwatchesFactory;
    protected $_pageFactory;

    protected $_postFactory;

    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \M2it\CustomSwatches\Model\PostFactory $postFactory,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_pageFactory = $pageFactory;
        $this->_postFactory = $postFactory;
        parent::__construct($context, $registry, $formFactory, $data);
        
    }

   
    protected function _construct()
    {
        parent::_construct();
        $this->setId('post_form');
        $this->setTitle(__('Post Information'));
    }

  
    protected function _prepareForm()
    {
       
        $model = $this->_coreRegistry->registry('custom_swatches');


        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data']]
        );

        $form->setHtmlIdPrefix('post_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('swatch_id', 'hidden', ['name' => 'swatch_id']);
        }

        $fieldset->addField(
            'title',
            'text',
            ['name' => 'title', 'label' => __('Swatch Name'), 'title' => __('Swatch Name'), 'required' => true]
        );

        $fieldset->addField(
            'img',
            'image',
            [
                'name' => 'img',
                'label' => __('Image'),
                'title' => __('Image'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'sort',
            'text',
            [
                'name' => 'sort',
                'label' => __('Sort'),
                'title' => __('Sort'),
                'required' => true,
                'class' => 'validate-number',
            ]
        );


        $fieldset->addField(
            'creases',
            'text',
            [
                'name' => 'creases',
                'label' => __('creases'),
                'title' => __('creases'),
                'required' => false,
                'class' => 'validate-number'
            ]
        );

         $fieldset->addField(
            'priceFinish',
            'text',
            [
                'name' => 'priceFinish',
                'label' => __('Finishing Price'),
                'title' => __('Finishing Price'),
                'required' => false,
            ]
        );

         $fieldset->addField(
            'priceSewing',
            'text',
            [
                'name' => 'priceSewing',
                'label' => __('Sewing Price'),
                'title' => __('Sewing Price'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'parent_id',
            'select',
            [
                'label' => __('parent'),
                'title' => __('parent'),
                'name' => 'parent_id',
                'required' => false,
                'options' => $this->swatchesList()
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );
        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        $fieldset->addField(
            'descirption',
            'editor',
            [
                'name' => 'descirption',
                'label' => __('descirption'),
                'title' => __('descirption'),
                'style' => 'height:4em',
                'required' => false
            ]
        );

        $fieldset->addField(
            'pattern',
            'editor',
            [
                'name' => 'pattern',
                'label' => __('Pattern'),
                'title' => __('Pattern'),
                'style' => 'height:4em',
                'required' => false
            ])->setAfterElementHtml('
        <div class="CustomSwatches-info">
            <span>Dostępne jednostki we wzorze:</span>
                 <ul>
                    
                    <li>#PP# - Cena za metr (pobierana automatycznie z ceny produktu)</li>
                    <li>#W# - Szerokość</li>
                    <li>#H# - Wysokość</li>
                    <li>#FP# - Cena za metr wykończenia (pobierana automatycznie z pola "cena wykończenia")</li>
                    <li>#SP# - Cena za metr szycia (pobierana automatycznie z pola "cena szycia")</li>
                    <li>#M# - wartość marszczenia materiału (pobierana automatycznie z pola "marszczenie")</li>
                </ul>
                   <span> 
                       Użycie na przykładzie Zakładki układane ręcznie (wpisujemy w pole wzór)
                    </span>
                    <br/><br/>
                    <span>
                        (#PP# * (#M# * #W# + 0.15)) + (#SP# * #W#) + ((#M# * #W# + 0.15) * #FP#)
                    </span>
                    <br/><br/>
            <span>W przypadku dodania opcji z przelotkami. Najpierw należy utworzyć ogólną opcję "Przelotki",
            a nastepnie tworzymy konkretną przelotkę np. "Przelotka zielona" i w polu "Rodzic" ustawiamy "Przelotki"</span>
                
        </div>'
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);



        return parent::_prepareForm();
    }

    private function swatchesList()
    {
        $swatchesList = array();
        $swatchesList[0] = __('none');
        $post = $this->_postFactory->create();
        $collection = $post->getCollection();
        foreach($collection as $item){
            
            $swatchesList[$item->getId()]=$item->getTitle();
            
        }

        return $swatchesList;
    }
}

