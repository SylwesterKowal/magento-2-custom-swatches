<?php
namespace M2it\CustomSwatches\Block\Adminhtml\Swatches;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
   
    protected $_coreRegistry = null;

    
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'swatch_id';
        $this->_blockGroup = 'M2it_CustomSwatches';
        $this->_controller = 'adminhtml_Swatches';

        parent::_construct();

        if ($this->_isAllowedAction('M2it_CustomSwatches::save')) {
            $this->buttonList->update('save', 'label', __('Save Swatch'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }

        if ($this->_isAllowedAction('M2it_CustomSwatches::swatch_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Swatch'));
        } else {
            $this->buttonList->remove('delete');
        }
         $this->buttonList->remove('reset');
    }

    
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('M2it_CustomSwatches')->getId()) {
            return __("Edit Swatch '%1'", $this->escapeHtml($this->_coreRegistry->registry('M2it_CustomSwatches')->getTitle()));
        } else {
            return __('New Swatch');
        }
    }

   
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('swatches/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}

