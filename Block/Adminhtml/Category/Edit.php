<?php
namespace M2it\CustomSwatches\Block\Adminhtml\Category;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{

    protected $_coreRegistry = null;

 
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }


    protected function _construct()
    {
        $this->_objectId = 'category_id';
        $this->_blockGroup = 'M2it_CustomSwatches';
        $this->_controller = 'adminhtml_Category';

        parent::_construct();

        if ($this->_isAllowedAction('M2it_CustomSwatches::saveCategory')) {
            $this->buttonList->update('save', 'label', __('Save Swatches Category'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }

        if ($this->_isAllowedAction('M2it_CustomSwatches::category_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Category'));
        } else {
            $this->buttonList->remove('delete');
        }
        $this->buttonList->remove('reset');
    }


    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('M2it_CustomSwatches')->getId()) {
            return __("Edit Category '%1'", $this->escapeHtml($this->_coreRegistry->registry('M2it_CustomSwatches')->getTitle()));
        } else {
            return __('New Category');
        }
    }

 
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }


    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('category/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}

