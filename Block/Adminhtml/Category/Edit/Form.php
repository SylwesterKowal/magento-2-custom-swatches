<?php
namespace M2it\CustomSwatches\Block\Adminhtml\Category\Edit;

use M2it\CustomSwatches\Block\SwatchesList;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

  
    protected $_systemStore;
    protected $_modelSwatchesFactory;
    protected $_pageFactory;
    protected $_postFactory;
    protected $_swatchesList;
    protected $_categoryCollection;
    protected $_storeManager;
    protected $_categoryPivotFactory;
    protected $_swatchesPivotFactory;
 
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \M2it\CustomSwatches\Model\PostFactory $postFactory,
        \M2it\CustomSwatches\Model\CategoryPivotFactory $categoryPivotFactory,
        \M2it\CustomSwatches\Model\SwatchesPivotFactory $swatchesPivotFactory,
        SwatchesList $swatchesList,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_pageFactory = $pageFactory;
        $this->_postFactory = $postFactory;
        $this->_categoryPivotFactory = $categoryPivotFactory;
        $this->_swatchesList = $swatchesList;
        $this->_swatchesPivotFactory = $swatchesPivotFactory;
        $this->_categoryCollection = $categoryCollection;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $registry, $formFactory,  $data);
        
    }

    
    protected function _construct()
    {
        parent::_construct();
        $this->setId('category_form');
        $this->setTitle(__('Category Information'));
    }

  
    protected function _prepareForm()
    {

       
        $model = $this->_coreRegistry->registry('custom_swatchesCategory');

        
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('category_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('category_id', 'hidden', ['name' => 'category_id']);
        }

        $fieldset->addField(
            'title',
            'text',
            ['name' => 'title', 'label' => __('Category Name'), 'title' => __('Category Name'), 'required' => true]
        );

        $fieldset->addField(
            'categoryType',
            'select',
            [
                'label' => __('Category Type'),
                'title' => __('Category Type'),
                'name' => 'categoryType',
                'required' => false,
                'options' => array(
                        1 => __('Firany'), 2 => __('Zasłony'))
            ]
        );

        $fieldset->addField(
            'parent_id',
            'multiselect',
            [
                'label' => __('swatches'),
                'title' => __('swatches'),
                'name' => 'parent_id',
                'required' => false,
                'values' =>  $this->_getExistingSwatchesCategories()
                

            ]
        );
        $model->setData('parent_id', $this->_getSelectedSwatches($model->getId()));


        $fieldset->addField(
            'magentoCategories_id',
            'multiselect',
            [
                'label' => __('category'),
                'title' => __('category'),
                'class' => 'col-md-6 col-sm-12',
                'name' => 'magentoCategories_id',
                'required' => false,
                'values' =>  $this->_getExistingMagentoCategories()
                

            ]
        );

        $model->setData('magentoCategories_id', $this->_getSelectedEntity($model->getId()));



        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);



        return parent::_prepareForm();
    }

     private function _getExistingSwatchesCategories()
    {
        $swatches = $this->_postFactory->create();
        $existingCategories = $swatches->getCollection()
        ->addFieldToFilter('parent_id', '0');

        $itemList = array();
        foreach($existingCategories as $_item)
        {
            $itemList[$_item->getId()]['label'] = $_item->getTitle();
            $itemList[$_item->getId()]['value'] = $_item->getId();
        }
        return $itemList;
    }

     private function _getExistingMagentoCategories()
    {
        $CategoryCollecton = $this->_categoryCollection->create()
        ->addAttributeToSelect('*')
        ->setStore($this->_storeManager->getStore())
        ->addAttributeToFilter('is_active','1');
        $categoryList = array();
        foreach($CategoryCollecton as $category)
        {
            $categoryList[$category->getId()]['label'] = $category->getName();
            $categoryList[$category->getId()]['value'] = $category->getId();

            foreach ($category->getParentCategories() as $parent) {
                
                    $categoryList[$category->getId()]['label'] .='  ['.$parent->getName().']';
                
            }
        }
        return $categoryList;
    }

    private function _getSelectedSwatches($categoryId)
    {
        $swatches = $this->_swatchesPivotFactory->create();
        $selectedCategory = $swatches->getCollection()
        ->addFieldToFilter('category_id', $categoryId);

         $itemList = array();
        foreach($selectedCategory as $_item)
        {
            $itemList[] = $_item->getData('swatch_id');

        }
        return $itemList;
    }

    private function _getSelectedEntity($categoryId)
    {
        $entity = $this->_categoryPivotFactory->create();
        $selectedEntity = $entity->getCollection()
        ->addFieldToFilter('category_id', $categoryId);

         $itemList = array();
        foreach($selectedEntity as $_item)
        {
            $itemList[] = $_item->getData('entity_id');

        }
        return $itemList;
    }
}

