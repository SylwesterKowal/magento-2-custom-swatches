<?php
namespace M2it\CustomSwatches\Block;

use M2it\CustomSwatches\Api\Data\SwatchesInterface;
use M2it\CustomSwatches\Model\ResourceModel\Swatches\Collection as SwatchesCollection;

class SwatchesList extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{
    
    protected $_postCollectionFactory;

    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \M2it\CustomSwatches\Model\ResourceModel\Swatches\CollectionFactory $postCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_postCollectionFactory = $postCollectionFactory;
    }

   
    public function getPosts()
    {
       
        if (!$this->hasData('swatches')) {
            $posts = $this->_postCollectionFactory
                ->create()
                ->addFilter('is_active', 1)
                ->addOrder(
                    SwatchesInterface::CREATION_TIME,
                    SwatchesCollection::SORT_ORDER_DESC
                );
            $this->setData('swatches', $posts);
        }
        return $this->getData('swatches');
    }

   
    public function getIdentities()
    {
        return [\M2it\CustomSwatches\Model\Post::CACHE_TAG . '_' . 'list'];
    }

}

