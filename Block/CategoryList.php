<?php
namespace M2it\CustomSwatches\Block;

use M2it\CustomSwatches\Api\Data\CategoryInterface;
use M2it\CustomSwatches\Model\ResourceModel\Category\Collection as CategoryCollection;

class CategoryList extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * @var \M2it\CustomSwatches\Model\ResourceModel\Post\CollectionFactory
     */
    protected $_CategoryCollectionFactory;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \M2it\CustomSwatches\Model\ResourceModel\Post\CollectionFactory $CategoryCollectionFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \M2it\CustomSwatches\Model\ResourceModel\Post\CollectionFactory $CategoryCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_CategoryCollectionFactory = $CategoryCollectionFactory;
    }

    /**
     * @return \M2it\CustomSwatches\Model\ResourceModel\Post\Collection
     */
    public function getCategories()
    {
        // Check if posts has already been defined
        // makes our block nice and re-usable! We could
        // pass the 'posts' data to this block, with a collection
        // that has been filtered differently!
        if (!$this->hasData('category')) {
            $posts = $this->_CategoryCollectionFactory
                ->create()
                ->addFilter('is_active', 1)
                ->addOrder(
                    CategoryInterface::CREATION_TIME,
                    CategoryCollection::SORT_ORDER_DESC
                );
            $this->setData('category', $category);
        }
        return $this->getData('category');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\M2it\CustomSwatches\Model\Category::CACHE_TAG . '_' . 'list'];
    }

}

