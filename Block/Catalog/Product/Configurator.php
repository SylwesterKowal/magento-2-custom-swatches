<?php
namespace M2it\CustomSwatches\Block\Catalog\Product;
use M2it\CustomSwatches\Api\Data\SwatchesInterface;
use M2it\CustomSwatches\Model\ResourceModel\Swatches\Collection as SwatchesCollection;
use Magento\Catalog\Block\Product\ListProduct;

class Configurator extends \Magento\Framework\View\Element\Template
{    
    
    protected $_registry;
    protected $_categoryPivotFactory;
    protected $_swatchesPivotFactory;
    protected $_resourceModelCategory;
    protected $_postCollectionFactory;
    protected $_productFactory;
    protected $_listProductBlock;
    protected $_storeManager;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \M2it\CustomSwatches\Model\CategoryPivotFactory $categoryPivotFactory,
        \M2it\CustomSwatches\Model\SwatchesPivotFactory $swatchesPivotFactory,
        \M2it\CustomSwatches\Model\ResourceModel\Category $ResourceModelCategory,
        \M2it\CustomSwatches\Model\ResourceModel\Swatches\CollectionFactory $postCollectionFactory,
        \Magento\Catalog\Block\Product\ListProduct $listProductBlock,
       \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
        )
    {
            $this->_registry = $registry;
            $this->_categoryPivotFactory = $categoryPivotFactory;
            $this->_swatchesPivotFactory = $swatchesPivotFactory;
            $this->_resourceModelCategory = $ResourceModelCategory;
            $this->_postCollectionFactory = $postCollectionFactory;
            $this->_productFactory = $productFactory;
            $this->_listProductBlock = $listProductBlock;
           $this->_storeManager = $storeManager;
             parent::__construct($context, $data);
    }

    public function getProductCollection()
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
       $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->load();
        return $collection;
    }

    public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }
    
    private function getCurrentCategory()
    {        
        return $this->_registry->registry('current_category');
    }  

    public function getCurrentProductId()
    {        
        $product = $this->_registry->registry('current_product');
        return $product;
    }

    private function getEntityIdFromProduct(){
        $categories= $this->getCurrentProductId()->getCategoryCollection()->addAttributeToSelect('name');
        foreach ($categories as $c){
            $catArr[]=$c->getId();
        }

        return $catArr;
    }

    public function getRelationIds()
    {
        $idEntity = $this->getEntityIdFromProduct();
        $itemList = $this->getRelationIdsById($idEntity);
       // $categories = $this->_categoryPivotFactory->create();
        // $category = $categories->getCollection()
        // ->addFieldToFilter('entity_id', $idEntity);
        //  $itemList = array();
        // foreach($category as $_item)
        // {
        //     $itemList['category_id'] = $_item->getId();
        //     $itemList['entity_id'] = $_item->getEntityId(); 
        // }
        return $itemList;
    }
    
    private function getRelationIdsById($catArr)
    {
        $categories = $this->_categoryPivotFactory->create();
        $category = $categories->getCollection()
        ->addFieldToFilter('entity_id', $catArr);
         $itemList = array();

        foreach($category as $_item)
        {
            $itemList['category_id'] = $_item->getId();
            $itemList['entity_id'] = $_item->getEntityId(); 
        }
        return $itemList;
    }

    public function isVisible()
    {
        if(!$this->isProductVisible($this->getEntityIdFromProduct()))
        {
         return false;
        }
        return true;
    }
    public function isProductVisible($catArr)
    {
        if(!$this->getRelationIdsById($catArr))
        {
         return false;
        }
        return true;
    }


    public function getSwatchesByCategory()
    {
        //$categoryId = $this->getRelationIds()['category_id'];
        
        $categoryId = $this->getSwatchesCategory();
        $list = $this->_resourceModelCategory->getCategorySwatchesRelationId($categoryId);
        foreach ($list as $key) {
            $swatch=$this->getMainSwatch($key);
            $swatchList[]=$swatch;
            //return $swatch->getSort();
        }
  
      usort($swatchList, array($this, 'sortSwatches'));

      return $swatchList;

    }

    public function sortSwatches($a, $b)
    {
      
        if ($a->getSort() == $b->getSort()) {
            return 0;
        }
        
            return ($a->getSort() < $b->getSort()) ? -1 : 1;
        
    }

    public function getSwatchesCategory()
    {
        $categoryId = $this->getRelationIds()['category_id'];
        return $categoryId;
    }

    protected function getSwatchArrayById($swatchId)
    {
        $posts = $this->_postCollectionFactory
                ->create()
                ->addFilter('swatch_id', $swatchId)
                ->addOrder(
                    SwatchesInterface::CREATION_TIME,
                    SwatchesCollection::SORT_ORDER_DESC
                );
            $this->setData('swatch', $posts);
        return $this->getData('swatch');
    }

    public function getSwatchById($swatchId)
    {
       foreach ($this->getSwatchArrayById($swatchId) as $swatch) {
            $getSwatch = $swatch;
        }
        return $getSwatch;
    }

    public function getMainSwatch($swatchId)
    {
        $posts = $this->_postCollectionFactory
            ->create()
            ->addFilter('is_active', 1)
            ->addFilter('parent_id', 0)
            ->addFilter('swatch_id', $swatchId)
             ->addOrder(
                     SwatchesInterface::CREATION_TIME,
                    SwatchesCollection::SORT_ORDER_DESC
                );
        foreach ($posts as $key) {
            $swatch = $key;
        }
        $this->setData('swatches', $swatch);

        return $this->getData('swatches');

    }

    public function getChildSwatch($swatchId)
    {
            $posts = $this->_postCollectionFactory
                ->create()
                ->addFilter('is_active', 1)
                ->addFilter('parent_id', $swatchId)
                ->addOrder(
                    SwatchesInterface::CREATION_TIME,
                    SwatchesCollection::SORT_ORDER_DESC
                );
            $this->setData('swatchesParent', $posts);
        return $this->getData('swatchesParent');

    }

    public function isSwatchHaveParent($id_swatch)
    {
        foreach ($this->getChildSwatch($id_swatch) as $value) {
            $check = $value;
            break;    
        }
        
        if(isset($check)){
            return true;
        }
        return false;
    }



    public function getImg($urlImage)
    {
      $url = 'http://romanetka.pl/media/'.$urlImage;
     if(strlen($urlImage) < 1)
        {
            $url = 'http://romanetka.pl/media/nophoto.jpg';
        }
        return $url;
    
    }


    public function getSwatchesCategoryNameForFrontend($category_id)
    {
        $category = $this->_resourceModelCategory
                    ->getCategoryName($category_id);

      return $category;
    }

    public function getSwatchesCategoryType($category_id)
    {
        $category = $this->_resourceModelCategory
                    ->getCategoryType($category_id);

      return $category[0];
    }


}
?>