<?php
namespace M2it\CustomSwatches\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\ObjectManager;

class CustomFields implements ObserverInterface
{
  
  
    protected $_request;
    
    public function __construct(
       	RequestInterface $request,
        Json $serializer = null
        ) 
    {    
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
        $this->_request = $request;
    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if($this->_request->getParam('selectedSwatch') == null){
            return false;
        }
        if ($this->_request->getFullActionName() == 'checkout_cart_add') {
            $product = $observer->getProduct();
            $additionalOptions = [];
            $additionalOptions[] = array(
                'label' => "Szerokość",
                'value' => $price = $this->_request->getParam('width'),
            );
            $additionalOptions[] = array(
                'label' => "Długość",
                'value' => $price = $this->_request->getParam('height'),
            );
            $additionalOptions[] = array(
                'label' => "Wybrana opcja",
                'value' => $price = $this->_request->getParam('selectedSwatch'),
            );
            $additionalOptions[] = array(
                'label' => "randomField",
                'value' => rand(),
            );
            $observer->getProduct()->addCustomOption('additional_options', $this->serializer->serialize($additionalOptions));
        }
    }
}