<?php
namespace M2it\CustomSwatches\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer as EventObserver;
use M2it\CustomSwatches\Helper\CalcHelper;

class CustomPrice implements ObserverInterface
{

    public function __construct(
    \Magento\Framework\App\RequestInterface $request,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Framework\View\LayoutInterface $layout,
    CalcHelper $calcHelper
    )
    {
        $this->_request = $request;
        $this->_calcHelper = $calcHelper;
    }


    public function execute(\Magento\Framework\Event\Observer $observer) {
        $productPrice = $observer->getProduct()->getPrice();
        if($this->_request->getParam('selectedSwatchId') == null){
            return true;
        }
        $item = $observer->getEvent()->getData('quote_item');         
        $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
        $price = $this->getPrice($observer->getProduct()->getId());
        $item->setCustomPrice($price);
        $item->setOriginalCustomPrice($price);
        $item->getProduct()->setIsSuperMode(true);
    }

    private function getPrice($productId)
    {
        
        $height = $this->_request->getParam('height');
        $width = $this->_request->getParam('width');
        $creases = $this->_request->getParam('creases');
        $swatchId = $this->_request->getParam('selectedSwatchId');

        return $price = $this->_calcHelper->calcPrice($width, $height , $creases, $productId, $swatchId, true);
    }


}

