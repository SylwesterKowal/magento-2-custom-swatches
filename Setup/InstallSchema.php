<?php 
namespace M2it\CustomSwatches\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('m2it_customswatches_swatches'))
            ->addColumn(
                'swatch_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'swatch ID'
            )
            ->addColumn('title', Table::TYPE_TEXT, 255, ['nullable' => false], 'Swatch title')
            ->addColumn('descirption', Table::TYPE_TEXT, '2M', [], 'Swatch description')
            ->addColumn('priceSewing', Table::TYPE_TEXT, null, ['nullable' => false], 'Swatch price')
            ->addColumn('priceFinish', Table::TYPE_TEXT, null, ['nullable' => false], 'Swatch price')
            ->addColumn('creases', Table::TYPE_TEXT, null, ['nullable' => true], 'Swatch creases')
            ->addColumn('img', Table::TYPE_TEXT, 255, ['nullable' => false], 'Swatch image')
            ->addColumn('sort', Table::TYPE_TEXT, 255, ['nullable' => false], 'Swatch sort')
            ->addColumn('is_active', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1'], 'Is Swatch Active?')
            ->addColumn('parent_id', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '0'], 'swatchs parent')
            ->addColumn('pattern', Table::TYPE_TEXT, '2M', [], 'pattern')
            ->addColumn('creation_time', Table::TYPE_TIMESTAMP, null, ['nullable' => false], 'Creation Time')
            ->addColumn('update_time', Table::TYPE_TIMESTAMP, null, ['nullable' => false], 'Update Time')
            ->addIndex($installer->getIdxName('title', ['title']), ['title'])
            ->setComment('M2it CustomSwatches Swatches');

        $installer->getConnection()->createTable($table);


        $table = $installer->getConnection()
            ->newTable($installer->getTable('m2it_customswatches_category'))
            ->addColumn(
                'category_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'swatch ID'
            )
            ->addColumn('title', Table::TYPE_TEXT, 255, ['nullable' => false], 'Category title')
            ->addColumn('categoryType', Table::TYPE_TEXT, null, ['nullable' => false], 'Category type')
            ->addColumn('creation_time', Table::TYPE_TIMESTAMP, null, ['nullable' => false], 'Creation Time')
            ->addColumn('update_time', Table::TYPE_TIMESTAMP, null, ['nullable' => false], 'Update Time')
            ->addIndex($installer->getIdxName('title', ['title']), ['title'])
            ->setComment('M2it CustomSwatches Category');

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()
            ->newTable($installer->getTable('m2it_customswatches_category_swatch'))
            ->addColumn('category_id', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '0'], 'swatchs parent')
            ->addForeignKey(
            $installer->getFkName('m2it_customswatches_category_swatch', 'category_id', 'm2it_customswatches_category', 'category_id'),
            'category_id',
            $installer->getTable('m2it_customswatches_category'),
            'category_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE)
            
            ->addColumn('swatch_id', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '0'], 'swatchs parent')
            ->addForeignKey(
            $installer->getFkName('m2it_customswatches_category_swatch', 'swatch_id', 'm2it_customswatches_swatches', 'swatch_id'),
            'swatch_id',
            $installer->getTable('m2it_customswatches_swatches'),
            'swatch_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE);

        $installer->getConnection()->createTable($table); 

        $table = $installer->getConnection()
            ->newTable($installer->getTable('m2it_customswatches_category_entity'))
            ->addColumn('category_id', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '0'], 'swatchs parent')
            ->addForeignKey(
            $installer->getFkName('m2it_customswatches_category_entity', 'category_id', 'm2it_customswatches_category', 'category_id'),
            'category_id',
            $installer->getTable('m2it_customswatches_category'),
            'category_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE)
            
            ->addColumn('entity_id', Table::TYPE_INTEGER, null, ['nullable' => false, 'default' => '0', 'unsigned' => true], 'swatchs parent')
            ->addForeignKey(
            $installer->getFkName('m2it_customswatches_category_entity', 'entity_id', 'catalog_category_entity', 'entity_id'),
            'entity_id',
            $installer->getTable('catalog_category_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE);

        $installer->getConnection()->createTable($table); 

        $installer->endSetup();
    }

}

