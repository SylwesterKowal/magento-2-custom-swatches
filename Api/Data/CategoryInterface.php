<?php
namespace M2it\CustomSwatches\Api\Data;


interface CategoryInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const CATEGORY_ID     = 'category_id';
    const TITLE           = 'title';
    const CATEGORYTYPE    = 'categoryType';


    public function getId();
    public function getTitle();

  
    public function setId($id);
    public function setTitle($title);

    public function setCategoryType($categoryType);
    public function getCategoryType();
}

