<?php
namespace M2it\CustomSwatches\Api\Data;


interface SwatchesInterface
{
  
    const SWATCH_ID     = 'swatch_id';
    const IMG           = 'img';
    const TITLE         = 'title';
    const PRICEFINISH   = 'priceFinish';
    const PRICESEWING   = 'priceSewing';
    const DESCRIPTION   = 'descirption';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const IS_ACTIVE     = 'is_active';
    const PARENT_ID     = 'parent_id';
    const CREASES       = 'creases';
    const PATTERN       = 'pattern';
    const SORT          = 'sort';


    public function getId();

    public function getImg();

    public function getTitle();

    public function getSort();

    public function getPriceFinish();

    public function getPriceSewing();

    public function getDescription();

    public function getCreationTime();

    public function getUpdateTime();

    public function getPattern();

    public function isActive();

    public function setId($id);

    public function setImg($img);

    public function setTitle($title);

    public function setSort($sort);

    public function getCreases();

    public function setDescription($descirption);

    public function setCreationTime($creationTime);

    public function setUpdateTime($updateTime);
 
    public function setIsActive($isActive);
    
    public function setPriceFinish($priceFinish);

    public function setPriceSewing($priceSewing);

    public function setParentId($parent_id);

    public function setPattern($pattern);

    public function setCreases($creases);
}

