<?php
namespace M2it\CustomSwatches\Model;

use Magento\Framework\DataObject\IdentityInterface;

class SwatchesPivot  extends \Magento\Framework\Model\AbstractModel
{

	const SWATCH_ID = 'id';
    
    protected function _construct()
    {
        $this->_init('M2it\CustomSwatches\Model\ResourceModel\SwatchesPivot');
    }


    public function getId()
    {
        return $this->getData(self::SWATCH_ID);
    }


}

