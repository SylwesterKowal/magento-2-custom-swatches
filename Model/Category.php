<?php
namespace M2it\CustomSwatches\Model;

use  M2it\CustomSwatches\Api\Data\CategoryInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Category  extends \Magento\Framework\Model\AbstractModel implements CategoryInterface, IdentityInterface
{

  

    const CACHE_TAG = 'category_tag';

    protected $_cacheTag = 'category_tag';


    protected $_eventPrefix = 'category_tag';


    protected function _construct()
    {
        $this->_init('M2it\CustomSwatches\Model\ResourceModel\Category');
    }



    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


    public function getId()
    {
        return $this->getData(self::CATEGORY_ID);
    }

    public function getTitle()
    {
        return $this->getData(self::TITLE);
    } 

     
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    public function getCategoryType()
    {
        return $this->getData(self::CATEGORYTYPE);
    } 

     
    public function setCategoryType($categoryType)
    {
        return $this->setData(self::CATEGORYTYPE, $categoryType);
    }


}

