<?php
namespace M2it\CustomSwatches\Model;

use Magento\Framework\DataObject\IdentityInterface;

class CategoryPivot  extends \Magento\Framework\Model\AbstractModel
{

    const ENTITY = 'category_id';
    
    protected function _construct()
    {
        $this->_init('M2it\CustomSwatches\Model\ResourceModel\CategoryPivot');
    }


    public function getEntityId()
    {
        return $this->getData(self::ENTITY);
    }

    public function getId()
    {
        return $this->getData(self::ENTITY);
    }

    
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

}

