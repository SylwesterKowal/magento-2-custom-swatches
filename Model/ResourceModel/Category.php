<?php
namespace M2it\CustomSwatches\Model\ResourceModel;


class Category extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected $_date;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('m2it_customswatches_category', 'category_id');
    }


    public function getCategorySwatchesRelationId($category_id)
    {
        $adapter = $this->getConnection();

        $select = $adapter->select()->from(
            $this->getTable('m2it_customswatches_category_swatch'),
            'swatch_id'
        )->where(
            'category_id = ?',
            (int)$category_id
        );

        return $adapter->fetchCol($select);
    
    }

        public function getCategoryName($category_id)
    {
        $adapter = $this->getConnection();

     $select = $adapter->select()->from(
            $this->getTable('m2it_customswatches_category'),
            'title'
        )->where(
            'category_id = ?',
            (int)$category_id
        );   
        return $adapter->fetchCol($select);
    }

    public function getCategoryType($category_id)
    {
        $adapter = $this->getConnection();

     $select = $adapter->select()->from(
            $this->getTable('m2it_customswatches_category'),
            'categoryType'
        )->where(
            'category_id = ?',
            (int)$category_id
        );   
        return $adapter->fetchCol($select);
    }



   


}

