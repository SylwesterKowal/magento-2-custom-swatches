<?php 
namespace M2it\CustomSwatches\Model\ResourceModel\Swatches;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'swatch_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('M2it\CustomSwatches\Model\Post', 'M2it\CustomSwatches\Model\ResourceModel\Swatches');
    }

}

