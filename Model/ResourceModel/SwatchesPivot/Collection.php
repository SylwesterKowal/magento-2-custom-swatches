<?php 
namespace M2it\CustomSwatches\Model\ResourceModel\SwatchesPivot;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('M2it\CustomSwatches\Model\SwatchesPivot', 'M2it\CustomSwatches\Model\ResourceModel\SwatchesPivot');
    }

}

