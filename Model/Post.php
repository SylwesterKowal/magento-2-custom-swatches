<?php
namespace M2it\CustomSwatches\Model;

use  M2it\CustomSwatches\Api\Data\SwatchesInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Post  extends \Magento\Framework\Model\AbstractModel implements SwatchesInterface, IdentityInterface
{


    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    const CACHE_TAG = 'swatch_tag';


    protected $_cacheTag = 'swatch_tag';


    protected $_eventPrefix = 'swatch_tag';


    protected function _construct()
    {
        $this->_init('M2it\CustomSwatches\Model\ResourceModel\Swatches');
    }



    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getId()
    {
        return $this->getData(self::SWATCH_ID);
    }

    public function getImg()
    {
        return $this->getData(self::IMG);
    }
    
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    } 

    public function getSort()
    {
        return $this->getData(self::SORT);
    } 

    public function getPriceFinish()
    {
        return $this->getData(self::PRICEFINISH);
    }

    public function getPriceSewing()
    {
        return $this->getData(self::PRICESEWING);
    }

    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }


    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    public function getCreases()
    {
        return $this->getData(self::CREASES);
    }
 
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    public function getParentId()
    {
        return $this->getData(self::PARENT_ID);
    }

    public function getPattern()
    {
        return $this->getData(self::PATTERN);
    }
 
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }


    public function setId($id)
    {
        return $this->setData(self::SWATCH_ID, $id);
    }

 


    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    public function setSort($sort)
    {
        return $this->setData(self::SORT, $sort);
    }

  
    public function setPriceFinish($priceFinish)
    {
        return $this->setData(self::PRICEFINISH, $priceFinish);
    } 

    public function setPriceSewing($priceSewing)
    {
        return $this->setData(self::PRICESEWING, $priceSewing);
    }

  
    public function setImg($img)
    {
        return $this->setData(self::IMG, $img);
    }


    public function SetDescription($descritpion)
    {
        return $this->setData(self::DESCRIPTION, $descritpion);
    }

  
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

 
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    } 

 
    public function setParentId($parent_id)
    {
        return $this->setData(self::PARENT_ID, $parent_id);
    }

    public function setCreases($creases)
    {
        return $this->setData(self::CREASES, $creases);
    }

    public function setPattern($pattern)
    {
        return $this->setData(self::PATTERN, $pattern);
    }

}

