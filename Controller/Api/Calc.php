<?php
namespace M2it\CustomSwatches\Controller\Api;
 
use M2it\CustomSwatches\Helper\CalcHelper; 
use Magento\Framework\App\Action\Context;
 
class Calc extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_calcHelper;
    protected $productRepository;
 
    public function __construct(Context $context, 
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        CalcHelper $calcHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository)
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_calcHelper = $calcHelper;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $resultPage = $this->resultJsonFactory->create();
      
        if ($this->getRequest()->isAjax()) 
        {
            $price = $this->getPrice();
            $quantity = $this->getQuantity();
            $product = $this->productRepository->getById($this->getRequest()->getPost('materialId'));
            $stockItem = $product->getExtensionAttributes()->getStockItem();
            if ($stockItem->getQty()<$quantity){
                return $resultPage->setData(['status' => 'error' , 'type'=>'qty', 'quantity' => $stockItem->getQty()]);
            }
            return $resultPage->setData(['status' => 'OK' ,'price' => $price, 'quantity' => $quantity]);
        }
    }

    private function getPrice()
    {
        $height = $this->getRequest()->getPost('height');
        $width = $this->getRequest()->getPost('width');
        $creases = $this->getRequest()->getPost('creases');
        $materialPrice = $this->getRequest()->getPost('materialId');
        $swatchId = $this->getRequest()->getPost('swatchId');
        return $price = $this->_calcHelper->calcPrice($width, $height, $creases, $materialPrice, $swatchId);
    }

    private function getQuantity()
    {
        $width = $this->getRequest()->getPost('width');
        $creases = $this->getRequest()->getPost('creases');
        $quantity =  $this->_calcHelper->calcQuantity($width, $creases);
        return $quantity;
    }

}