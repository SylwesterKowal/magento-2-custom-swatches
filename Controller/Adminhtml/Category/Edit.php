<?php
namespace M2it\CustomSwatches\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
   
    protected $_coreRegistry = null;

    
    protected $resultPageFactory;

  
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('M2it_CustomSwatches::saveCategory');
    }

    
    protected function _initAction()
    {
       
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('M2it_CustomSwatches::CategoryList')
            ->addBreadcrumb(__('Categories'), __('Categories'))
            ->addBreadcrumb(__('Manage Categories Swatchs'), __('Manage Categories Swatchs'));
        return $resultPage;
    }

   
    public function execute()
    {
        $id = $this->getRequest()->getParam('category_id');
        $model = $this->_objectManager->create('M2it\CustomSwatches\Model\Category');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This category no longer exists.'));
                
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('custom_swatchesCategory', $model);

        
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Category') : __('New Category'),
            $id ? __('Edit Category') : __('New Category')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Category'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('Category'));

        return $resultPage;
    }
}

