<?php
namespace M2it\CustomSwatches\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('M2it_CustomSwatches::saveCategory');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {     

        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \M2it\CustomSwatches\Model\Post $model */
            $model = $this->_objectManager->create('M2it\CustomSwatches\Model\Category');

            $id = $this->getRequest()->getParam('category_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'CustomSwatches_category_prepare_save',
                ['category' => $model, 'request' => $this->getRequest()]
            );
            try {
                $model->save();

                $swatchesId = $model->save()->getData('parent_id');
                $categoryId = $model->save()->getData('category_id');
                
                $magentoCategoriesId = $model->save()->getData('magentoCategories_id');
                
                $this->beforeSaveRelationToPivot($categoryId);
                
                foreach ($swatchesId as $k => $swatchId) {
                    $this->saveRelationToPivot($categoryId, $swatchId, $table='swatch');
                }
                foreach ($magentoCategoriesId as $k => $magentoCategoryId) {
                    $this->saveRelationToPivot($categoryId, $magentoCategoryId, $table='entity');
                }


                $this->messageManager->addSuccess(__('You saved this Category'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['category_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the category.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['category_id' => $this->getRequest()->getParam('category_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }

    protected function saveRelationToPivot($category_id, $swatchOrEntity_id, $table)
    { 
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        if($table == 'swatch'){
            $sql = "INSERT INTO m2it_customswatches_category_swatch (category_id, swatch_id) VALUES ('".$category_id."', '".$swatchOrEntity_id."')";
        }else{
            $sql = "INSERT INTO m2it_customswatches_category_entity (category_id, entity_id) VALUES ('".$category_id."', '".$swatchOrEntity_id."')";
        }
      
        $connection->query($sql);
    }

    protected function beforeSaveRelationToPivot($category_id)
    {
      $this->deleteEntityFromPivot($category_id);
      $this->deleteSwatchesFromPivot($category_id);
    }
    
    protected function deleteEntityFromPivot($category_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql = "DELETE FROM m2it_customswatches_category_entity WHERE category_id='".$category_id."' ";

        $connection->query($sql);
    }

    protected function deleteSwatchesFromPivot($category_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql = "DELETE FROM m2it_customswatches_category_swatch WHERE category_id='".$category_id."' ";

        $connection->query($sql);
    }

    
}

