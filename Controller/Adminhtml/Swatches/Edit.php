<?php
namespace M2it\CustomSwatches\Controller\Adminhtml\Swatches;

use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
    
    protected $_coreRegistry = null;

    
    protected $resultPageFactory;

  
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

  
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('M2it_CustomSwatches::save');
    }

 
    protected function _initAction()
    {
     
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('M2it_CustomSwatches::content')
            ->addBreadcrumb(__('Swatch'), __('Swatch'))
            ->addBreadcrumb(__('Manage Swatch'), __('Manage Swatch'));
        return $resultPage;
    }


    public function execute()
    {
        $id = $this->getRequest()->getParam('swatch_id');
        $model = $this->_objectManager->create('M2it\CustomSwatches\Model\Post');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This swatch no longer exists.'));
   
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('custom_swatches', $model);

   
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Swatch') : __('New Swatch'),
            $id ? __('Edit Swatch') : __('New Swatch ')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Swatch'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Swatch'));

        return $resultPage;
    }
}

