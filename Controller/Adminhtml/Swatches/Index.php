<?php
namespace M2it\CustomSwatches\Controller\Adminhtml\Swatches;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

   
    protected $resultPageFactory;

  
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

   
    public function execute()
    {
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('M2it_CustomSwatches::content');
        $resultPage->addBreadcrumb(__('Custom Swatches'), __('Custmom Swatches'));
        $resultPage->addBreadcrumb(__('Manage Custom Swatches'), __('Manage Custom Swatches'));
        $resultPage->getConfig()->getTitle()->prepend(__('Custom Swatches'));

        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('M2it_CustomSwatches::content');
    }


}

