<?php
namespace M2it\CustomSwatches\Controller\Adminhtml\Swatches;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('M2it_CustomSwatches::save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \M2it\CustomSwatches\Model\Post $model */
            $model = $this->_objectManager->create('M2it\CustomSwatches\Model\Post');

            $id = $this->getRequest()->getParam('swatch_id');
            if ($id) {
                $model->load($id);

            }

            if(isset($data['img']['delete']) && !$data['img']['delete']){
                unset($data['img']);
            }else{
                $data['img']=$model->getImg();
            }
            
            $model->setData($data);

            $this->_eventManager->dispatch(
                'CustomSwatches_post_prepare_save',
                ['post' => $model, 'request' => $this->getRequest()]
            );
            try {
                $profileImage = $this->getRequest()->getFiles('img');
                $fileName = ($profileImage && array_key_exists('name', $profileImage)) ? $profileImage['name'] : null;
                if ($profileImage && $fileName) {
                    try {
                        /** @var \Magento\Framework\ObjectManagerInterface $uploader */
                        $uploader = $this->_objectManager->create(
                            'Magento\MediaStorage\Model\File\Uploader',
                            ['fileId' => 'img']
                        );

                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapterFactory */
                        $imageAdapterFactory = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')
                            ->create();
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(true);
                        $uploader->setAllowCreateFolders(true);
                        /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                            ->getDirectoryRead(DirectoryList::MEDIA);

                        $result = $uploader->save(
                            $mediaDirectory
                                ->getAbsolutePath('M2it/CustomSwatches')
                        );

                        //$data['profile'] = 'Modulename/Profile/'. $result['file'];
                        $model->setImg('M2it/CustomSwatches'.$result['file']); //Database field name
                    } catch (\Exception $e) {

                        if ($e->getCode() == 0) {
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                }

                $model->save();

                $this->messageManager->addSuccess(__('You saved this Swatch.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['swatch_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {

                $this->messageManager->addException($e, __('Something went wrong while saving the swatch.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['swatch_id' => $this->getRequest()->getParam('swatch_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}

