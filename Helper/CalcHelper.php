<?php
namespace M2it\CustomSwatches\Helper;

use M2it\CustomSwatches\Block\Catalog\Product\Configurator;
use \Magento\Catalog\Model\ProductFactory;

Class CalcHelper{

	protected $_product;
	protected $_swatchData;

	public function __construct(
			Configurator $swatchData,
			ProductFactory $productFactory)
	{
		$this->_swatchData = $swatchData;
		$this->_product = $productFactory;
	}

	public function calcPrice($width, $height, $creases, $productId, $swatchId, $unitPrice=false)
	{
		$swatch = $this->_swatchData->getSwatchById($swatchId);
		$parentId = $swatch->getParentId();
		$materialPrice = $this->getProductPrice($productId);
		$swatchPriceFinish = $swatch->getPriceFinish();
		$swatchPriceSewing = $swatch->getPriceSewing();
		$width = $width / 100;
		$height = $height / 100;

		$pattern = $swatch->getPattern();

		$replace = array
		(
			'#PP#' => $materialPrice,
			'#W#' => $width,
			'#H#' => $height,
			'#FP#' => $swatchPriceFinish,
			'#SP#' => $swatchPriceSewing,
			'#M#' => $creases
		);
		// echo $pattern."<br>";
		$sum = str_replace(array_keys($replace), $replace, $pattern);
		// die ($sum);
			$summary = eval('return '.$sum.';');


		if ($unitPrice){
			$summary = $summary / $this->calcQuantity($width*100, $creases);
		}	
		return $summary;
	}

	public function calcQuantity($width, $creases)
	{
		$quantity = ($width * $creases)/100;

		return $quantity;
	}


	private function getProductPrice($productId)
	{
		$product = $this->_product->create();
	    $productPriceById = $product->load($productId)->getPrice();
		return $productPriceById;
	}



}