<?php $product = $block->getCurrentProductId(); ?>

<div class="container">

<?php if($block->isVisible()){?>

	<div class="col-md-4">

<?php		
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$listBlock = $objectManager->get('\Magento\Catalog\Block\Product\ListProduct');
$addToCartUrl =  $listBlock->getAddToCartUrl($product); ?>

		<form class="product_addtocart_form_<?php echo($product->getId())?>" action="<?php echo $addToCartUrl; ?>" method="post" id="product_addtocart_form">
		    <div class="sizes">
		    <?php echo $block->getBlockHtml('formkey')?>
					<input id="width"  type="text" name="width" value="" />
					<input id="height" type="text" name="height" value="" />
					<input id="price" type="text" name="price" value="" />
					<input id="materialId" type="hidden" name="materialId" value="<?= $product->getId()?>" />
					<input id="selectedSwatchId" type="hidden" name="selectedSwatchId" value="" />
					<input id="creases" type="hidden" name="creases" value="" />
					<input id="selectedSwatch" type="hidden" name="selectedSwatch" value="test" />
				    <input type="number"
				                       name="qty"
				                       id="qty"
				                       maxlength=""
				                       value=""
				                       class="hidden"
				                       title="<?php /* @escapeNotVerified */ echo __('Qty') ?>"
				                       />
				</div>
		       <button type="submit"
		               title="Add to Cart"
		               class="action tocart primary">
		               <span>Add to Cart</span>
		        </button>
		 </form>
		 <span id="error"></span>

	</div>

	<div class="CustomSwatches col-md-8">

		<?php foreach ($block->getSwatchesByCategory() as $idSwatch) { ?>

			<?php foreach ($block->getMainSwatch($idSwatch) as $swatch) { ?>

				<div style="background-image: url('<?= $block->getImg($swatch->getImg()) ?>')" class="swatch col-md-4 <?= $block->isSwatchHaveParent($idSwatch) ? 'parent' : 'click' ?>" id="swatch_<?= $swatch->getId() ?>">

					<div data-title="<?= $swatch->getTitle() ?>" class="swatchTitle">
						<h3><?= $swatch->getTitle() ?></h3>
					</div>
					<div class="swatchImage">
						<img src="" />
					</div>
					<div class="swatchCreases" data-creases="<?= $swatch->getCreases() ?>">
						<p>Marszczenie: <?= $swatch->getCreases() ?> </p>
					</div>
					<div class="swatchDescription">
						<p> <?= $swatch->getDescription() ?> </p>
					</div>
					<?php if(!$block->isSwatchHaveParent($idSwatch)){ ?>
						<div class="swatchPrice">
							<p id="swatchPrice_<?= $swatch->getId() ?>" data-id="<?= $swatch->getId() ?>">Cena za metr: <?= $swatch->getPrice() ?></p>
						</div>
					<?php } ?>

				</div>

				<?php if($block->isSwatchHaveParent($idSwatch)){ ?>
					<div class="swatchChild col-md-12 hidden" id="swatchChild_<?= $swatch->getId() ?>">
						
						<?php foreach ($block->getChildSwatch($idSwatch) as $parentSwatch) { ?>

						<div style="background-image: url('<?= $block->getImg($parentSwatch->getImg()) ?>')" class="swatchParent col-md-4 click">

							<div data-title="<?= $parentSwatch->getTitle() ?>" class="swatchChildTitle">
								<h3><?= $parentSwatch->getTitle() ?></h3>
							</div>
							<div class="swatchChildImage">
								<img src="" />
							</div>
							<div class="swatchChildCreases" data-creases="<?= $parentSwatch->getCreases() ?>">
								<p>Marszczenie: <?= $parentSwatch->getCreases() ?> </p>
							</div>
							<div class="swatchChildDescription">
								<p> <?= $parentSwatch->getDescription() ?> </p>
							</div>

							<div class="swatchChildPrice">
								<p id="swatchPrice_<?= $parentSwatch->getId() ?>"  data-id="<?= $parentSwatch->getId() ?>">Cena za metr: <?= $parentSwatch->getPrice() ?></p>
							</div>

						</div>	

						<?php } ?>

					</div>

				<?php } ?>
				
			<?php } ?>

		<?php } ?>

	</div>

<?php }?>

</div>

<script type="text/javascript">
require(['jquery', 'jquery/ui'], function($){

$('.CustomSwatches').append($('.swatchChild'));

	var materialId = '<?= $product->getId()?>';
	var swatchId;
	var swatchName;
	var height;
	var width;
	var creases;
	var summary;
	var maxHeight = '<?= ($product->getCustomAttribute('max_height')) ?  $product->getCustomAttribute('max_height')->getValue() : ''  ?>';
	var maxWidth = '<?= ($product->getCustomAttribute('max_width')) ?  $product->getCustomAttribute('max_width')->getValue() : ''  ?>';
	var ajaxRequest=false;

	$(".CustomSwatches .swatch").click(function() {

		if($(this).hasClass('parent')){

			$("#selectedSwatchId").val('');
			$("#selectedSwatch").val('');
			$(this).addClass('active').siblings().removeClass('selected active');
			validation();
			

		}else{

	    	$(this).addClass('selected').siblings().removeClass('selected active');
	    	 swatchId = $(this).find(".swatchPrice p").attr('data-id');
	    	 swatchName =  $(this).find(".swatchTitle").attr('data-title');
	    	 creases =  $(this).find(".swatchCreases").attr('data-creases');
	    	validation();
		}

		if($(".CustomSwatches .parent").hasClass('active')){

			$(".swatchChild").removeClass('hidden');

			$(".CustomSwatches .swatchChild .swatchParent").click(function() {

		   		$(this).addClass('selected').siblings().removeClass('selected');
		   		swatchId = $(this).find(".swatchChildPrice p").attr('data-id');	
		   		swatchName = $(this).find(".swatchChildTitle").attr('data-title');
		   		creases =  $(this).find(".swatchChildCreases").attr('data-creases');	
		   		$("#selectedSwatchId").val(swatchId);
		   		$("#selectedSwatch").val(swatchName);
		   		validation();
	   		});

		}else{

			$(".swatchChild").addClass('hidden');
	   		$(".swatchParent").removeClass('selected');
	   		$("#selectedSwatchId").val(swatchId);
	   		$("#selectedSwatch").val(swatchName);
	   		$("#creases").val(creases);
	   		validation();
	   		
		}
		height = $('#height').val();
		width =$('#width').val();		
		validation();	
	});


	function validation()
	{
		

		var error=false;

		if($('#width').val() == '') error = 'podaj szerokość firany';
		if($('#width').val() > maxWidth && $('#width').val() < 10 ) error = 'Tkanina zbyt szeroka (dozwolona wartość mieści się miedzy 10 cm a '+maxWidth+' cm)';
		if($('#height').val() == '') error = 'podaj wysokość firany';
		if($('#height').val() > maxHeight && $('#height').val() < 10) error = 'Tkanina zbyt szeroka (dozwolona wartość mieści się miedzy 10 cm a '+maxHeight+' cm)';
		if($('#selectedSwatchId').val() == '' && $('#selectedSwatchId').val() == '' ) error = 'wybierz opcję firanki';

		if(!error)
		{ 
			//ajax(width, height, creases, materialId,swatchId );	
		}
	
		$('#error').text(error);
	}


	function ajax(width, height, creases, materialId, swatchId)
	{
        $(document).on('click', '.CustomSwatches .swatch',function() {
            var param = 'height='+height+'&width='+width+'&creases='+creases+'&materialId='+materialId+'&swatchId='+swatchId;
                if (ajaxRequest){
                	ajaxRequest.abort();
                }
                ajaxRequest=$.ajax({
                    showLoader: true,
                    url: '/swatches/Api/Calc',
                    data: param,
                    type: "POST",
                    dataType: 'json'
                }).done(function (data) {
                    $("#price").val(data);
                });
        });
	}
});
   
</script>